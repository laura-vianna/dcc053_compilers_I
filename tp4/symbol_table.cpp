// Retirado de https://homepages.dcc.ufmg.br/~mariza/Cursos/CompiladoresI/Geral/CodigoTS/codigoTS-C.html

#include "symbol_table.h"

struct tabela_s {
    char nome[name_max];   /* Contem o nome do Simbolo */
    int nivel;       /* Contem o nivel do Simbolo relacionado */
    char atributo[name_max]; /* Contem o atributo do  relacionado */

} TabelaS[100];     /* Vetor de struct que contem a tabela de simbolos */

int L;        /* inteiro que contem o indice do ultimo elemento da Tabela de Simbolos */        

/******************************************************************************
Function implementations
(Este arquivo foi baseado nas funcoes de TODO fornecidos no site)
https://homepages.dcc.ufmg.br/~mariza/Cursos/CompiladoresI/Geral/CodigoTS/codigoTS-C.html
******************************************************************************/

void init_symbol_table(void) {
    L = 0;               /* Considera-se que a primeira posicao da tabela eh a de indice 0. L eh o final da arvore binaria */
}

void Erro(int num) {
    switch (num) {
    case 1: cout << "\tErro: Tabela de Simbolos cheia" << endl;
            break;

    case 2: cout << "\tErro: Elemento nao encontrado\n" << endl;
            break;

    case 3: cout << "\tErro: Este item ja foi inserido\n" << endl;
            break;
    }
}

/* Pesquisa o simbolo "x" e retorna o indice da Tabela de simbolos onde ele se encontra */
int get_entry(char x[name_max]) {
    int k, achou, aux;

    k = L;
    achou = 0;

    // Procura o simbolo na tabela
    while ((k > 0) && (achou == 0)) {
        k--;

        aux = strcmp(TabelaS[k].nome, x);
        if (aux == 0)
            achou = 1;
    }

    // Se achou
    if (achou == 1) {
        cout << "\t\t\tIndice: " << k << endl;

        return (k);  /* Retorna o indice no vetor TabelaS do elemento procurado*/
    }

    // Se nao achou
    Erro(2);
    return(0);
}

/* Instala o simbolo "X" com o atributo atribut na Tabela de Simbolos */
void install_symbol(char X[name_max], char atribut[name_max]) {
    int achou, k, aux;
    
    // Se a tabela estiver cheia
    if (L == NMax + 1) {
        Erro(1);
        return;
    }

    // Procura o simbolo na tabela
    achou = 0;
    k = L;
    while (k > 0) {
        k--;

        aux = strcmp(TabelaS[k].nome,X);
        if (aux == 0) {
            achou = 1;
        }
    }

    // Se nao achou
    if (achou == 0) {

        // Salva o atributo
        aux = strlen(atribut);
        for (k = 0; k<= aux-1; k++)
            TabelaS[L].atributo[k] = atribut[k];

        // Salva o nome
        aux = strlen(X);
        for (k = 0; k<=(aux-1); k++)
            TabelaS[L].nome[k] = X[k];

        L++;
    }
}

void print_symbol_table() {
    int i;

    cout << endl << "---Tabela de simbolos---" << endl;
    for (i = 0; i <= L-1; i++) {
        cout << endl << i << " - Nome: " << TabelaS[i].nome << endl;
        cout << "Atributo: " << TabelaS[i].atributo << endl;
    }
}
